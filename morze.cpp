/* 
 * File:   morze.cpp
 * Author: Janis
 *
 * Created on sestdiena, 2012, 7 janv?ris, 19:59
 */

#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <fstream>

using namespace std;

/*
 * 
 */
class node {
public:
    node *left;
    node *right;
    char data;

    node(char str) {
        this->data = str;
        this->left = this->right = NULL;
    }

};

class Sistema {
    fstream in, out;
    char *inFileName;
    char *outFileName;
public:
    node *root;
    node *current;
    node *invalid;
    char pr;

    Sistema(char* inFile, char* outFile) {
        invalid = new node('!');
        inFileName = inFile;
        outFileName = outFile;
        pr='|';
        //first = NULL;
        //last = NULL;
        //current= NULL;
        buildTree();
        this->rewind();
        Read();

    }

    void buildTree() {
        root = new node(' ');
        /** Kreisais Koks*/
        root->left = new node('T');
        root->left->left = new node('M');
        root->left->left->left = new node('O');
        root->left->left->left->left = new node('!');
        root->left->left->left->left->left = new node('0');
        root->left->left->left->left->right = new node('9');
        root->left->left->left->right = new node('!');
        root->left->left->left->right->right = new node('8');
        root->left->left->left->right->right->right = new node(':');
        root->left->left->right = new node('G');
        root->left->left->right->left = new node('Q');
        root->left->left->right->right = new node('Z');
        root->left->left->right->right->right = new node('7');
        root->left->left->right->right->left = new node('!');
        root->left->left->right->right->left->left = new node(',');
        root->left->right = new node('N');
        root->left->right->left = new node('K');
        root->left->right->left->left = new node('Y');
        root->left->right->left->right = new node('C');
        root->left->right->right = new node('D');
        root->left->right->right->left = new node('X');
        root->left->right->right->left->right = new node('/');
        root->left->right->right->right = new node('B');
        root->left->right->right->right->right = new node('6');
        root->left->right->right->right->right->left = new node('-');
        /** Labais koks*/
        root->right = new node('E');
        root->right->left = new node('A');
        root->right->left->left = new node('W');

        root->right->left->left->left = new node('J');
        root->right->left->left->left->left = new node('1');
        root->right->left->left->left->left->right = new node(' ');
        root->right->left->left->right = new node('P');
        root->right->left->right = new node('R');
        root->right->left->right->left = new node('!');
        root->right->left->right->left->right = new node('!');
        root->right->left->right->left->right->left = new node('.');
        root->right->left->right->right = new node('L');
        root->right->right = new node('I');
        root->right->right->left = new node('U');
        root->right->right->left->left = new node('!');
        root->right->right->left->left->left = new node('2');
        root->right->right->left->left->right = new node('!');
        root->right->right->left->left->right->right = new node('?');
        root->right->right->left->right = new node('F');
        root->right->right->right = new node('S');
        root->right->right->right->left = new node('V');
        root->right->right->right->left->left = new node('3');
        root->right->right->right->right = new node('H');
        root->right->right->right->right->left = new node('4');
        root->right->right->right->right->right = new node('5');




    }

    void OpenFiles() {
        in.open(inFileName, ios::in);
        out.open(outFileName, ios::out);
    }

    void printChar(node *q) {
        out << q->data;
    }

    void rewind() {
        this->current = this->root;
    }

    void parse(char c) {
        if (c == '|') {
            
            if (this->current == NULL) {
                this->printChar(this->invalid);
                this->rewind();
                return;
            }
            if (this->root->data == this->current->data && this->pr!='|') {
                this->printChar(this->root);
                this->pr=c;
                this->rewind();
                return;
            } else if(this->pr!='|'){
                this->printChar(this->current);
                this->pr=c;
                this->rewind();
                return;
            }
        } else if (c == '.') {
            if (this->current != NULL) {
                this->current = this->current->right;
                this->pr=c;
            }
        } else if (c == '-') {
            if (this->current != NULL) {
                this->current = this->current->left;
                this->pr=c;
            }
        }
    }

    void Read() {
        this->OpenFiles();
        char i;
        while (!in.eof()) {
            in >> i;
            parse(i);
            // cout<<i;
        }
        if (this->current==NULL || this->current->data != this->root->data) {
            this->printChar(this->invalid);
        }

    }
};

int main(int argc, char** argv) {
    Sistema Moreze("morze.in", "morze.out");
    return 0;
}


